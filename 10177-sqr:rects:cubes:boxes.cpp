/*-------------------------------------------------------
 *
 * Actividad de programación: 10177 - (2/3/4)-D Sqr/Rects/Cubes/Boxes?
 * Fecha: 1 Febrero, 2017
 * Autor: A01700651 Andres Raygadas
 *
 *------------------------------------------------------*/

#include <iostream>
#include <cmath>
using namespace std;

int main(int argc, const char * argv[]) {
    int n;
    
    while(cin>>n){
        long long s1, s2, s3, r1, r2, r3;
        s1 = s2 = s3 = r1 = r2 = r3 = 0;
        
        // s1, s2, s3
        for (int i = 1; i <= n; i++){
            s1 += i * i;
            s2 += i * i * i;
            s3 += i * i * i * i;
        }
        
        // r1
        for (int i = 1; i <= n; i++){
            for (int j = 1; j <= n; j++){
                if ( i != j ){
                    r1 += (long long)(i) * (long long)(ceil(j));
                }
            }
        }
        
        // s2
        for (int i = 1; i <= n; i++){
            for (int j = 1; j <= n; j++){
                for (int k = 1; k <= n; k++){
                    if(!(i == j && j == k)){
                        r2 += (long long)i * (long long)j * (long long)k;
                    }
                }
            }
        }
        
        // s3
        for (int i = 1; i <= n; i++){
            for (int j = 1; j <= n; j++){
                for (int k = 1; k <= n; k++){
                    for (int l = 1; l <= n; l ++){
                        if(!(i == j && j == k && k == l)){
                            r3 += (long long)(i) * (long long)(j) * (long long)(k) * (long long)(l);
                        }
                    }
                }
            }
        }
        
        
        cout << s1 << " " << r1 << " " << s2 << " " << r2 << " " << s3 << " " << r3 << endl;
        
    }
                    
    return 0;
}
