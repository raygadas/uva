/*------------------------------------------------------
 * Actividad de programación: Problema 10340 - All in all
 * Fecha: 09-Abril-2017
 * Autor: A01700651 Andres Raygadas
 * Run Time: .000
 *------------------------------------------------------*/

#include <iostream>
#include <stdio.h>
using namespace std;

int main(){
  string s, t;

  while (cin >> s >> t){
    bool subsequence = false;
    string::iterator t_char;
    string::iterator s_char = s.begin();

    for (t_char = t.begin(); t_char < t.end(); t_char++){
      if (*s_char == *t_char) {
        s_char++;
        if (s_char == s.end()) {
          subsequence = true;
          break;
        }
      }
    }

    if (subsequence == true) cout << "Yes\n";
    else cout << "No\n";

  }
}
