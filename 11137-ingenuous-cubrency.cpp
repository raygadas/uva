/*------------------------------------------------------
 * Actividad de programación: Problema 11137
 * Fecha: 16-Abril-2017
 * Autor: A01700651 Andres Raygadas
 * Run Time: .000
 *------------------------------------------------------*/

# include <iostream>
# include <stdio.h>
# define MAX 10000
using namespace std;

long long int possible_ways[MAX];

int cube(int i){
  return i*i*i;
}

int main(void){

  // Initialize all possible ways at 0
  for (int i = 1; i < MAX; i++){
    possible_ways[i] = 0;
  }
  // Allows all amounts to have at least one possible way (Using only coins of denomination = 1)
  possible_ways[0] = 1;

  // Find all the possible ways for each amount less than MAX
  for (int i = 1; i <= 21; i++){
    // Coins denominations are equal to the values from 1 to 21 cubed
    int coin = cube(i);
    for (int j = coin; j < MAX; j++){
      possible_ways[j] += possible_ways[j-coin];
    }
  }

  int amount;
  while(cin >> amount){
    cout << possible_ways[amount] << endl;
  }
}
