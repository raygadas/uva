
/*-------------------------------------------------------
 *
 * Actividad de programación: Problema 679 de UVA
 * Fecha: 27-Marzo-2017
 * Autor: A01700651 Andres Raygadas
 *
 *------------------------------------------------------*/

#include <iostream>
#include <cmath>
using namespace std;

int main(int argc, const char * argv[]) {
    int test_cases;
    cin >> test_cases;
    
    while(test_cases--){
        int depth, pos = 1;
        float ith_ball;
        
        cin >> depth >> ith_ball;
        
        while (--depth){
            if (int(ith_ball) % 2 == 0){
                pos = (pos * 2) + 1;
            }
            else{
                pos = pos * 2;
            }
            
            ith_ball = ceil(float(ith_ball)/2);
        }
        
        cout << pos << endl;
    }
}

