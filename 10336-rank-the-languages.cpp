/*------------------------------------------------------
 * Actividad de programación: Problema 10336
 * Fecha: 25-Abril-2017
 * Autor: A01700651 Andres Raygadas
 * Run Time: .000
 *------------------------------------------------------*/


#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <vector>
using namespace std;
int H, W;
char map[100][100];

struct Pair {
  char language;
  int states;

  Pair(char l, int s){
    language = l;
    states = s;
  }

};

bool comparePairs(Pair a, Pair b){
  if (a.states > b.states)
    return true;

  else if (a.states < b.states)
    return false;

  else
    return a.language < b.language;
}

void depth_first_search(int i, int j, char language){
  if (i < 0 ||  j < 0 || i == H || j == W) return;
  if (map[i][j] == language) {
    map[i][j] = '*';
    depth_first_search(i-1, j, language);
    depth_first_search(i+1, j, language);
    depth_first_search(i, j-1, language);
    depth_first_search(i, j+1, language);
  }
  else return;
}

int main(void){
  int N;
  cin >> N;
  for (int world_num = 1; world_num <= N; world_num++){
    cin >> H >> W;
    vector<char> languages;
    vector<Pair> answer;

    for ( int i = 0; i < H; i++ ){
      for ( int j = 0; j < W; j++){
        char c;
        cin >> c;
        map[i][j] = c;
        if ( find(languages.begin(), languages.end(),c) == languages.end() ){
          languages.push_back(c);
        }
      }
    }

    for (int k = 0; k < languages.size(); k ++){
      int states = 0;
      for ( int i = 0; i < H; i++ ){
        for ( int j = 0; j < W; j++){
          if ( map[i][j] == languages[k] ) {
            depth_first_search(i, j, languages[k]);
            states++;
          }
        }
      }
      answer.push_back(Pair(languages[k], states));
    }

    sort(answer.begin(), answer.end(), comparePairs);

    cout << "World #" << world_num << endl;
    for (int i = 0; i < answer.size(); i++){
      cout << answer[i].language << ": " << answer[i].states << endl;
    }

  }
}
