/*------------------------------------------------------
 * Actividad de programación: Problema 957 - Popes de UVA
 * Fecha: 09-Abril-2017
 * Autor: A01700651 Andres Raygadas
 * Run Time: .030
 *------------------------------------------------------*/
 
#include <iostream>
#include <stdio.h>
using namespace std;

int binarySearchLastOccurrence(int arr[], int low, int high, int key);

int main ()
{
    int period;

    while ( scanf ("%d", &period) != EOF ) {
        // Se disminuye el periodo porque el primer año es inclusive
        period--;

        int num_popes;
        cin >> num_popes;

        int years[num_popes];
        for (int i = 0; i < num_popes; i++){
          cin >> years[i];
        }

        int size = 0;
        int j = 0, to = 0, from = 0;
        int max_size = 0;

        // Busca el ultimo año de eleccion que sea <= a año actual + periodo
        for (int i = 0; i < num_popes; i++)
        {
          j = binarySearchLastOccurrence(years, i, num_popes, years[i] + period);
          size = j - i + 1;

          // Actualiza las variables maximas
          if ( size > max_size ) {
            max_size = size;
            from = years[i];
            to = years[j];
          }
        }
        cout << max_size << " " << from << " " << to << endl;
  }
}

// http://www.studyalgorithms.com/array/find-the-index-of-last-occurrence-of-an-element-in-a-sorted-array/
int binarySearchLastOccurrence(int arr[], int low, int high, int key) {
  int mid = 0;
  if(high >= low)
  {
      mid = (low + high) / 2;
      if((mid == high && arr[mid] == key) || (arr[mid] <= key && arr[mid+1] > key)){
          return mid;
        }
      else if (arr[mid] <= key)
          return binarySearchLastOccurrence(arr, mid+1, high, key);
      else
          return binarySearchLastOccurrence(arr, low, mid-1, key);
  }
  return -1;
}
