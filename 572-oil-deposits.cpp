/*------------------------------------------------------
 * Actividad de programación: Problema 572
 * Fecha: 23-Abril-2017
 * Autor: A01700651 Andres Raygadas
 * Run Time: .000
 *------------------------------------------------------*/
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <vector>
using namespace std;
int m, n;
char grid[100][100];

void depth_first_search(int i, int j){
  if (i < 0 ||  j < 0 || i == m || j == n) return;
  if (grid[i][j] == '@') {
    grid[i][j] = '*';
    depth_first_search(i-1, j);
    depth_first_search(i-1, j-1);
    depth_first_search(i-1, j+1);
    depth_first_search(i+1, j);
    depth_first_search(i+1, j-1);
    depth_first_search(i+1, j+1);
    depth_first_search(i, j-1);
    depth_first_search(i, j+1);
  }
  else return;
}

int main(void){

  while ( scanf ("%d %d", &m, &n) && m != 0 ) {
    int oil_deposits = 0;


    for ( int i = 0; i < m; i++ ){
      for ( int j = 0; j < n; j++){
        cin >> grid[i][j];
      }
    }


    for ( int i = 0; i < m; i++ ){
      for ( int j = 0; j < n; j++){
        if ( grid[i][j] == '@' ) {
          depth_first_search(i, j);
          oil_deposits++;
        }
      }
    }


    cout << oil_deposits << endl;


  }

}
