
/*-------------------------------------------------------
 *
 * Actividad de programación: Maximum Product - 11059 de UVA
 * Fecha: 29 de enero de 2017
 * Autor: A01700651 Andres Raygadas
 *
 *------------------------------------------------------*/

#include <iostream>
#include <vector>
using namespace std;

int main(int argc, const char * argv[]) {
    int case_num = 1, N;
    while(cin >> N){
        vector<int> elements(N);
        long long max_p = 0;

        for(int i = 0; i < N; i++){
            cin >> elements[i];
        }

        for(int i = 0; i < elements.size(); i++){
            for (int j = i; j < N; j++){
                long long p = 1;
                for (int k = i; k <= j; k++){
                    p *= elements[k];
                    max_p = max(p, max_p);
                }
            }
        }

        cout << "Case #" << case_num << ": The maximum product is " << max_p << ".\n\n";
        case_num++;
    }

    return 0;
}
