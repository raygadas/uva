/*------------------------------------------------------
 * Actividad de programación: Problema 10474 de UVA
 * Fecha: 01-Marzo-2017
 * Autor: A01700651 Andres Raygadas
 *
 *------------------------------------------------------*/
 

#include <iostream>
#include <algorithm>
using namespace std;

int b_search(int *array, int l, int h, int d)
{
    while( l <= h)
    {
        int m = (l+h)/2;
        //if element is found
        if( array[m] == d )
        {
            //have we reached the beginning of the array
            //or middle and it's previous elements are equal?
            //search the left half
            if( m > 0 && array[m] == array[m-1])
            {
                h= m-1;
            }
            else
                return m+1; //we found the first occurence return it.
        }
        //following two conditions are similar to the binary search
        else if ( array[m] < d)
        {
            l = m+1;
        }
        else
        {
            h = m-1;
        }
    }
    return -1;
}

int main(int argc, const char * argv[]) {

    int num_marbles = 0, num_queries = 0, cont = 0;

    while(cin >> num_marbles >> num_queries){
        if (num_marbles == 0 && num_queries == 0){ break; }
        // Leer las marbles
        cont ++;
        int marbles[num_marbles];
        for (int i = 0; i < num_marbles; i++)
            cin >> marbles[i];

        //Ordenarlas
        sort(marbles, marbles+num_marbles);


        /*for (int i = 0; i < num_marbles; i++)
        cout << marbles[i] << " " ;*/


        cout << "CASE# " + to_string(cont) + ":" << endl;

        int result, q;

        // Hacer la busqueda
        for (int i = 0 ; i < num_queries; i++){
            cin >> q;
            result = -1;

            result = b_search(marbles, 0, num_marbles-1, q);
            if (result != -1)
                cout << to_string(q) + " found at " + to_string(result) << endl;

            else
                cout << to_string(q) + " not found" << endl;
        }
    }
    return 0;
}
