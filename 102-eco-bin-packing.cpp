/*-------------------------------------------------------
 *
 * Actividad de programación: Ecological Bin Packing
 * Fecha: 30 Enero 2017
 * Autor: A01700651 Andres Raygadas
 *
 *------------------------------------------------------*/

#include <iostream>
#include <vector>
using namespace std;

int main(int argc, const char * argv[]) {
    vector<string> c(6);
    int aB, aG, aC, bB, bG, bC, cB, cG, cC;
    c = {"BCG", "BGC", "CBG", "CGB", "GBC", "GCB"};
    
    
    while(cin >> aB >> aG >> aC >> bB >> bG >> bC >> cB >> cG >> cC){
        int min;
        int r[6];
        int indice = 0;
    
        r[0] = bB + cB + aC + cC + aG + bG;
        
        r[1] = bB + cB + aG + cG + aC + bC;
        
        r[2] = bC + cC + aB + cB + aG + bG;
        
        r[3] = bC + cC + aG + cG + aB + bB;
        
        r[4] = bG + cG + aB + cB + aC + bC;
        
        r[5] = bG + cG + aC + cC + aB + bB;
        
        min = r[0];
        
        for(int i = 1; i < 6; i++){
            if ( r[i] < min){
                indice = i;
                min = r[i];
            }
        }
        
        cout << c[indice] << " " << min << endl;
    }
    
}
