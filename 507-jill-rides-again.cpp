/*------------------------------------------------------
 * Actividad de programación: Problema 507
 * Fecha: 12-Abril-2017
 * Autor: A01700651 Andres Raygadas
 * Run Time: .190
 *------------------------------------------------------*/
#include <iostream>
#include <stdio.h>
using namespace std;

int main(void){
  int b, r, s, i;
  int max_i, max_j, max_sum, start, sum, n;

  cin >> b;

  for (r = 1; r <= b; r++){
    cin >> s;

    max_j = 0;
    max_i = 0;
    max_sum = 0;
    sum = 0;
    start = 1;

    for (i = 1; i < s; i++){
      cin >> n;

      // When sum becomes negative, it starts again
      if (sum < 0){
        sum = 0;
        start = i;
      }

      sum += n;

      // Update max's when sum > or == to max_sum and when current diff (i-j) is >
      if (sum > max_sum || ( (max_sum == sum) && (i+1 - start > max_j - max_i)) ){
        max_i = start;
        max_j = i+1;
        max_sum = sum;
      }
    }

    if (max_sum > 0)
      cout << "The nicest part of route " << r << " is between stops " << max_i << " and " << max_j << endl;
    else
      cout << "Route " << r << " has no nice parts" << endl;

  }
}
