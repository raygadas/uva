/*------------------------------------------------------
 * Actividad de programación: Problema 108
 * Fecha: 16-Abril-2017
 * Autor: A01700651 Andres Raygadas
 * Run Time: .020
 *------------------------------------------------------*/
#include <iostream>
#include <stdio.h>
#include <climits>
#include <algorithm>
using namespace std;

int main(void){
  int N, n;
  while (cin >> N){
    int sums[N+1][N+1];

    // Matrix with sums
    sums[0][0] = 0;
    for (int i = 1; i <= N; i++){
      sums[i][0] = 0;
      for (int j = 1; j <= N; j++){
        sums[0][j] = 0;
        cin >> n;
        sums[i][j] = n + sums[i-1][j] + sums[i][j-1] - sums[i-1][j-1];
      }
    }

    // Checks for maximum sum in matrix created above
    int sum = 0, max_sum = INT_MIN;
    for (int s_row = 1; s_row <= N; s_row++){
      for (int s_col = 1; s_col <= N; s_col++){
        for (int e_row = s_row; e_row <= N; e_row++){
          for (int e_col = s_col; e_col <= N; e_col++){
            sum = sums[e_row][e_col] - sums[s_row-1][e_col] - sums[e_row][s_col-1] + sums[s_row-1][s_col-1];
            max_sum = max(max_sum, sum);
        }
      }
    }
  }
    cout <<  max_sum << endl;
  }
}
