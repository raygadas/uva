/*------------------------------------------------------
 * Actividad de programación: Problema 10020
 * Fecha: 10-Abril-2017
 * Autor: A01700651 Andres Raygadas
 * Run Time: .070
 *------------------------------------------------------*/
#include <iostream>
#include <stdio.h>
#include <vector>
#include <algorithm>
using namespace std;

struct Pair{
  int L, R;

  Pair(int given_L, int given_R){
    L = given_L;
    R = given_R;
  }
  bool operator < (const Pair & other) const
   {
       return L < other.L;
   }
};

int main(void) {
  int cases;
  cin >> cases;

   for (int c = 0; c < cases; c++ )
   {
    int M;
    int L, R;
    int L_prev = 0, R_prev = 0;

    vector<Pair> result;
    vector<Pair> pairs;

    cin >> M;

    while (true) {
      cin >> L >> R;
      if (L == 0 && R == 0)
        break;
      pairs.push_back(Pair(L, R));
    }

    // Sorts pairs with respect to L
    sort(pairs.begin(), pairs.end());


    for (int i = 0; i < pairs.size(); i++){

      if (pairs[i].R > R_prev) {

        // Substitutes starting segment
        if (pairs[i].L <= 0){
          L_prev = pairs[i].L; R_prev = pairs[i].R;
          result.clear();
          result.push_back(Pair(pairs[i].L, pairs[i].R));
        }

        // Substitutes an existing segmento
        else if (pairs[i].L <= L_prev){
          if(!result.empty()){
            result.pop_back();
          }
          L_prev = pairs[i].L; R_prev = pairs[i].R;
          result.push_back(Pair(pairs[i].L, pairs[i].R));
        }

        // Adds another segment
        else if (pairs[i].L <= R_prev && R_prev < M){
          if (!result.empty() && pairs[i].L <= result[result.size() - 2].R){
            result.pop_back();
          }
          L_prev = pairs[i].L; R_prev = pairs[i].R;
          result.push_back(Pair(pairs[i].L, pairs[i].R));
      }
    }
  }

    if (result.empty() || result.back().R < M ){
      cout << 0 << endl;
    }

    else{
      cout << result.size() << endl;
      for (int i = 0; i < result.size(); i++) {
        cout << result[i].L << " " << result[i].R << endl;
      }
    }
    cout << endl;
  }
}
