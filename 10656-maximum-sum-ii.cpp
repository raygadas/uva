/*------------------------------------------------------
 * Actividad de programación: Problema 10656
 * Fecha: 09-Abril-2017
 * Autor: A01700651 Andres Raygadas
 * Run Time: .000
 *------------------------------------------------------*/
#include <iostream>
#include <stdio.h>
using namespace std;

int main () {
    int N, num;
    cin >> N;

    while ( N != 0 ){
      string result;

      while (N--){
         cin >> num;
         if (num != 0) {
            result += to_string(num) + " ";
         }
      }

      if (result.size() == 0 )
         cout << "0";
      else
         result.pop_back();
         cout << result << endl;

      cin >> N;

   }

}
