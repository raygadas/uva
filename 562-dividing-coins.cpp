/*------------------------------------------------------
 * Actividad de programación: Problem 562
 * Fecha: 19-Abril-2017
 * Autor: A01700651 Andres Raygadas
 * Run Time: .020
 *------------------------------------------------------*/
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
using namespace std;

int knapSack(int m, int W, int A[]){
  int M[m+1][W+1];

  for(int i = 0; i <= W; i++){
    M[0][i] = 0;
  }

  for (int i = 1; i <= m; i++){
    M[i][0] = 0;
    int pos = i-1;
    for (int j = 1; j <= W; j++){
      if (A[pos] > j) M[i][j] = M[i-1][j];
      else if (A[pos] <= j) M[i][j] = max(M[i-1][j], M[i-1][j-A[pos]] + A[pos]);
    }
  }
return M[m][W];
}

int main() {
  int n, m;
  cin >> n;

  while(n--){
    cin >> m;
    int A[m], sum = 0;
    for (int i = 0; i < m; i++){
      cin >> A[i];
      sum += A[i];
    }
    cout << sum-2*(knapSack(m, sum/2, A)) << endl;
  }
}
